#include <stdio.h>

const char dst[128] = "0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[1]^_`abcdefghijklmnopqrstuvwxyz";

void uint32_to_str(unsigned int x, char *s)
{
    unsigned short int k0 = x >> 24;
    unsigned short int k1 = (x >> 16) & 0xff;
    unsigned short int k2 = (x >> 8) & 0xff;
    unsigned short int k3 = x & 0xff;
    s[3] = dst[k0-48];
    s[2] = dst[k1-48];
    s[1] = dst[k2-48];
    s[0] = dst[k3-48];
    //s[4] = '\0';
}

int main(){
    unsigned int a,b,c,d;

    __asm__ __volatile__ ( \
    "pushq %%rbx        \n" \
    "xorq %%rcx,%%rcx   \n" \
    "cpuid              \n" \
    "movq %%rbx, %%rsi  \n" \
    "popq %%rbx         \n" : \
        "=a" (a), "=S" (b), "=c" (c), "=d" (d) : "a" (0));

    printf("eax=0\n");
    printf("a=0x%08x\nb=0x%08x\nc=0x%08x\nd=0x%08x\n\n",a,b,c,d);

    char name[13];
    uint32_to_str(b,name);
    uint32_to_str(c,name+8);
    uint32_to_str(d,name+4);
    name[12]=0;
    printf("%s\n\n",name);

    __asm__ __volatile__ ( \
    "pushq %%rbx        \n" \
    "xorq %%rcx,%%rcx   \n" \
    "cpuid              \n" \
    "movq %%rbx, %%rsi  \n" \
    "popq %%rbx         \n" : \
        "=a" (a), "=S" (b), "=c" (c), "=d" (d) : "a" (1));

    printf("eax=1\n");
    printf("a=0x%08x\nb=0x%08x\nc=0x%08x\nd=0x%08x\n\n",a,b,c,d);
}

